variable "root_subject" {
  type = map(any)

  default = {
    common_name         = "Root Authority"
    organization        = "Committee of Public Safety"
    organizational_unit = "Information Assurance"
    locality            = "Seattle"
    province            = "Duwamish Territory"
    country             = "Imperial Core"
  }
}

variable "intermediate_subject" {
  type = map(any)

  default = {
    common_name         = "Intermediate Authority"
    organization        = ""
    organizational_unit = ""
    locality            = ""
    province            = ""
    country             = ""
  }
}

variable "root_ca_validity_period_hours" {
  default = 24 * 365 * 3
}

variable "intermediate_ca_validity_period_hours" {
  default = 24 * 365 * 1
}
