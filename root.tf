resource "tls_private_key" "root" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_self_signed_cert" "root" {
  key_algorithm         = "RSA"
  private_key_pem       = tls_private_key.root.private_key_pem
  validity_period_hours = var.root_ca_validity_period_hours
  is_ca_certificate     = true

  allowed_uses = [
    "key_encipherment",
    "key_agreement",
    "digital_signature",
    "cert_signing",
    "crl_signing",
    "ocsp_signing",
    "server_auth",
    "client_auth",
  ]

  subject {
    common_name         = lookup(var.root_subject, "common_name")
    organization        = lookup(var.root_subject, "organization", "")
    organizational_unit = lookup(var.root_subject, "organizational_unit", "")
    locality            = lookup(var.root_subject, "locality", "")
    province            = lookup(var.root_subject, "province", "")
    country             = lookup(var.root_subject, "country", "")
  }
}
